# Send Image

Send an image from one device and receive it on another one. The script shows a live feed in a window, sends the image whenever a certain key is pressed, and displays the received image in another window.

This example uses the python [cv2](https://pypi.org/project/opencv-python/) and [imutils](https://github.com/jrosebr1/imutils/) libraries which are extremely useful for image manipulation in python but can be a bit tricky to use.

For more information about the network and to sign up got to [OpenRnD.co.uk](https://openrnd.co.uk)

## Installation

To use this example you will need the following:

- Python
- [Socket-io Client](http://https://python-socketio.readthedocs.io/en/latest/client.html)
```python
pip install "python-socketio[client]"
```
- [cv2](https://pypi.org/project/opencv-python/)
```python
pip install opencv-python
```
- [imutils](https://github.com/jrosebr1/imutils/)
```python
pip install imutils
```

## Usage

#### Create sendImage1
1. Create the device with device_id 'sendImage1' on the dashboard
2. Delete all text in the inbound_rules field
3. Create an outbound_rule with:
- command_id: "image_out",
- data: ["image"],
- recipient:
  - username: your username,
  - device_id: "receiveImage1",
  - command_id: "image_in",
  - data_map: [{"outbound_key":"image", "inbound_key":"image"}]

```json
[
    {
        "command_id": "image_out",
        "data": [
            "image"
        ],
        "rule_group": [
            {
                "recipients": [
                    {
                        "username": "***your username***",
                        "device_id": "receiveImage1",
                        "command_id": "image_in",
                        "typus": "device",
                        "data_map": [
                            {
                                "outbound_key": "image",
                                "inbound_key": "image"
                            }
                        ]
                    }
                ],
                "max_freq": "0",
                "last_run": "0"
            }
        ]
    }
]
```

![sendImage1](./sendImage1.PNG "sendImage1")
4. Click 'Create Device'
5. Download or copy the python file 'sendImage.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### Create receiveImage1
1. Create the device with device_id 'receiveImage1' on the dashboard.
2. Delete all text in the outbound_rules field
3. Create an inbound_rule with:
- command_id: "image_in",
- data: ["image"],
- accept:
  - username: your username,
  - device_id: "sendImage1",
  - command_id: "image_out"

```json
[
    {
        "command_id": "image_in",
        "data": [
            "image"
        ],
        "max_freq": "0",
        "last_run": "0",
        "accept": [
            {
                "username": "***your image***",
                "device_id": "sendImage1",
                "command_id": "image_out"
            }
        ]
    }
]
```

![receiveImage1](./receiveImage1.PNG "receiveImage1")
4. Click 'Create Device'
5. Download or copy the python file 'receiveImage.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### RUN
1. Make sure the devices are set to active on the dashboard
2. CD to your python file folder and run sendImage.py in a cmd window
```bash
python sendImage.py
```
3. CD to your python file folder and run receiveImage.py in **another cmd window**
```bash
python receiveImage.py
```
Once sendImage.py is running, there is a 2 second wait and then a window should open showing a live video stream. Click on this video stream window to make sure it is in focus and press the 't' key to take a photo. The image is sent over the system and should be shown in another window with title 'Receiver'.

Pressing the 'q' key with either image window in focus will cause it to close and the loop it is running in to stop. Close and restart the python script if you wish to use it again.

Inside receiveImage.py there is a function called runImageViewer() that waits for the image to be updated in the showFrame variable and then displays it. I have not found a way to get cv2.imshow to display an image without running it in a loop in this way.

## Contributing
If you are a multilingual programmer and manage to translate these python examples in to another language, or produce some samples/tutorials of your own, please send them to hello@openrnd.co.uk and we'll add them here!

We are looking to make a code and device sharing platform in the near future.

## Acknowledgment
- Our IOT Network uses SocketIO as a base
- Big thanks to [makeareadme](https://www.makeareadme.com/) for making it super easy to make this README

## License
[MIT](https://choosealicense.com/licenses/mit/)
