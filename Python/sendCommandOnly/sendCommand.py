import socketio
import time

###---Device Settings---###

##-- Update these values with the values set on the dashboard--##
device_settings = {'username': 'test@example.com', 'device_id': 'sendCommand1', 'device_pw': '1234', 'server_pw': '1234'}

###---End Device Settings---###
###---SocketIO Setup---###

sio = socketio.Client();
error = None

@sio.on('connect')
def connect():
    print('Connected')
    sio.emit('authentication', {'username': device_settings['username'], 'device_id': device_settings['device_id'], 'password': device_settings['device_pw']})

@sio.on('authenticated')
def authenticated(data):
	if (data['s_password'] != device_settings['server_pw']):
		error = 'Server password mismatch'
		sio.disconnect()
	else:
		print('Authenticated')
		action_out()

@sio.on('unauthorized')
def unauthorized(reason):
    print('%s %s' % ('unauthorized:', reason))
    error = reason
    sio.disconnect()

@sio.on('disconnect')
def disconnect():
    print('%s %s' % ('disconnect:', (error)))

###---End SocketIO Setup---###
###---OpenRnD Settings---###

##-- Update here to handle inbound rules--##
@sio.on('action_in')
def action_in(action):
    print('%s %s' % ('action_in: ', action))
    print('%s' % action['command_id'])

##-- Update here to handle outbound rules--##
def action_out():
	while True:
		print('Sending')
		sio.emit('action_out', { 'command_id': 'outbound_example' , 'data': {} })
		time.sleep(10)

###---End OpenRnD Settings---###
###---Connect to Network---###

sio.connect('https://staging.openrnd.co.uk')
#sio.connect('https://openrnd.co.uk')
#sio.connect('http://localhost:4200')

###---End Connect to Network---###
