# OpenRnD Samples

These code samples and tutorials should help you get started quickly in building devices for the OpenRnD IOT Network.

For more information about the network and to sign up got to [OpenRnD.co.uk](https://openrnd.co.uk)

## Languages Provided

At the moment we have tutorials and examples in the following languages:

- Python
- HTML/JS/CSS on localhost

It is possible to use a different language for your sender and receiver devices, for example sending data out from python running on a RaspberryPi and receiving it on a webpage running on localhost.

## Usage

The repository is split in to folders for each language, with subfolders for each example. An example contains code for two devices, a sender and receiver.

The subfolders are named based on what the example does, to make it easy to choose the one you need.

If you want to get started quickly choose 'sendCommandOnly' which has a pair of scripts: one for a device to connect to the Network and send a simple command with no data, and one for a device that receives this command.

Of the basic examples, the progression is as follows:
- sendCommandOnly
- sendCommandWithData
- sendImage
- sendImageStream

Each example subfolder has a README that acts as the tutorial for the example.

## Contributing
If you are a multilingual programmer and manage to translate these python examples in to another language, or produce some samples/tutorials of your own, please send them to hello@openrnd.co.uk and we'll add them here!

We are looking to make a code and device sharing platform in the near future.

## License
[MIT](https://choosealicense.com/licenses/mit/)
