import socketio
import time

import cv2
from imutils import resize
import numpy as np
import base64

###---Device Settings---###

##-- Update these values with the values set on the dashboard--##
device_settings = {'username': 'test@example.com', 'device_id': 'receiveImageStream1', 'device_pw': '1234', 'server_pw': '1234'}

###---End Device Settings---###
###---SocketIO Setup---###

sio = socketio.Client();
error = None

@sio.on('connect')
def connect():
    print('Device Connected')
    sio.emit('authentication', {'username': device_settings['username'], 'device_id': device_settings['device_id'], 'password': device_settings['device_pw']})

@sio.on('authenticated')
def authenticated(data):
	if (data['s_password'] != device_settings['server_pw']):
		error = 'Server password mismatch'
		sio.disconnect()
	else:
		print('Device Authenticated')
		main()

@sio.on('unauthorized')
def unauthorized(reason):
    print('%s %s' % ('unauthorized:', reason))
    error = reason
    sio.disconnect()

@sio.on('disconnect')
def disconnect():
    print('%s %s' % ('disconnect:', (error)))

###---End SocketIO Setup---###
###---OpenRnD Device Settings---###

##-- Update here to handle inbound rules--##
@sio.on('action_in')
def action_in(action):
	#print('%s %s' % ('action_in: ', action))
	print('%s' % action['command_id'])

	if action['command_id'] == 'imageStream_in':
		global showFrame
		dt = np.dtype('uint8')
		nparr = np.fromstring(action['data']['image'], dtype=dt) #Turn image data to numpy array
		showFrame1 = cv2.imdecode(nparr, 1) #decode image using cv2 (1 means colour)
		showFrame = resize(showFrame1, width=960) #Resize for viewing

##-- Update here to handle outbound rules--##
def action_out():
	#sio.wait() #Only use sio.wait if there is no logic loop running in the code
	pass

##--Add cutom functions here--##
def runImageViewer():
	global showFrame #Variable to store received image
	showFrame = None #Set to None initially
	while True:

		if showFrame is not None: #When showFrame is not none and image has been received
			cv2.imshow("Receiver", showFrame) #Show the image that is received in a new window with a title of 'Receiver'

		key = cv2.waitKey(1) & 0xff #Wait for any keypress while the image viewer is in focus (click in the image to ensure focus)
		if key == ord("q"): #If q pressed then exit loop and close image
			break

	cv2.destroyAllWindows() #close image

##--Main is run one the device is authenticated--##
def main():
	action_out()
	runImageViewer()

###---End OpenRnD Device Settings---###
###---Connect to Network---###

sio.connect('https://staging.openrnd.co.uk')
#sio.connect('https://openrnd.co.uk')
#sio.connect('http://localhost:4200')

###---End Connect to Network---###
