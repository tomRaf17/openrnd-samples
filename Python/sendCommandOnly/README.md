# Send Command Only

Send a command from one device and receive it on another one. The script sends the command every 10 seconds, no data is sent with the command.

For more information about the network and to sign up got to [OpenRnD.co.uk](https://openrnd.co.uk)

## Installation

To use this example you will need the following:

- Python
- [Socket-io Client](http://https://python-socketio.readthedocs.io/en/latest/client.html)

## Usage

#### Create sendCommand1
1. Create the device with device_id 'sendCommand1' on the dashboard
2. Delete all text in the inbound_rules field
3. Create an outbound_rule with:
- command_id: "outbound_example",
- data: [],
- recipient:
  - username: your username,
  - device_id: "receiveCommand1",
  - command_id: "inbound_example",
  - data_map: []

```json
[
    {
        "command_id": "outbound_example",
        "rule_group": [
            {
                "recipients": [
                    {
                        "username": "***your username***",
                        "device_id": "receiveCommand1",
                        "command_id": "inbound_example",
                        "typus": "device"
                    }
                ],
                "max_freq": "0",
                "last_run": "0"
            }
        ]
    }
]
```

![sendCommand1](./sendCommand1.PNG "sendCommand1")
4. Click 'Create Device'
5. Download or copy the python file 'sendCommand.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### Create receiveCommand1
1. Create the device with device_id 'receiveCommand1' on the dashboard.
2. Delete all text in the outbound_rules field
3. Create an inbound_rule with command_id = 'inbound_example', data array empty, accept set to your username, device_id 'sendCommand1' and command_id 'outbound_example'
- command_id: "inbound_example",
- data: [],
- accept:
  - username: your username,
  - device_id: "sendCommand1",
  - command_id: "outbound_example"

```json
[
    {
        "command_id": "inbound_example",
        "max_freq": "0",
        "last_run": "0",
        "accept": [
            {
                "username": "***your username***",
                "device_id": "sendCommand1",
                "command_id": "outbound_example"
            }
        ]
    }
]
```

![receiveCommand1](./receiveCommand1.PNG "receiveCommand1")
4. Click 'Create Device'
5. Download or copy the python file 'receiveCommand.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### RUN
1. Make sure the devices are set to active on the dashboard
2. CD to python file folder and run sendCommand.py in a cmd window
```bash
python sendCommand.py
```
3. CD to python file folder and run receiveCommand.py in another cmd window
```bash
python receiveCommand.py
```
On the sendCommand.py window you should see 'Sending' printed every 10 seconds, while on receiveCommand.py you should see the the full data received followed by the command_id received.

You should be able to see that the command_id received is 'inbound_example' while the command being sent out is 'outbound_example'. This forms the basics of mapping between devices.

## Contributing
If you are a multilingual programmer and manage to translate these python examples in to another language, or produce some samples/tutorials of your own, please send them to hello@openrnd.co.uk and we'll add them here!

We are looking to make a code and device sharing platform in the near future.

## Acknowledgment
- Our IOT Network uses SocketIO as a base
- Big thanks to [makeareadme](https://www.makeareadme.com/) for making it super easy to make this README

## License
[MIT](https://choosealicense.com/licenses/mit/)
