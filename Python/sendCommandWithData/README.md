# Send Command With Data

Send a command from one device and receive it on another one. The script sends the command every 10 seconds, with data that is mapped between the sender and receiver.

For more information about the network and to sign up got to [OpenRnD.co.uk](https://openrnd.co.uk)

## Installation

To use this example you will need the following:

- Python
- [Socket-io Client](http://https://python-socketio.readthedocs.io/en/latest/client.html)

## Usage

#### Create sendCommandData1
1. Create the device with device_id 'sendCommandData1' on the dashboard
2. Delete all text in the inbound_rules field
3. Create an outbound_rule with:
- command_id: "temp_out",
- data: ["temp"],
- recipient:
  - username: your username,
  - device_id: "receiveCommandData1",
  - command_id: "temp_in",
  - data_map: [{"outbound_key":"temp", "inbound_key":"temperature"}]

```json
[
    {
        "command_id": "temp_out",
        "data": [
            "temp"
        ],
        "rule_group": [
            {
                "recipients": [
                    {
                        "username": "***your username***",
                        "device_id": "receiveCommandData1",
                        "command_id": "temp_in",
                        "typus": "device",
                 "data_map": [
                            {
                                "outbound_key": "temp",
                                "inbound_key": "temperature"
                            }
                        ]
                    }
                ],
                "max_freq": "0",
                "last_run": "0"
            }
        ]
    }
]
```

![sendCommandData1](./sendCommandData1.PNG "sendCommandData1")
4. Click 'Create Device'
5. Download or copy the python file 'sendCommandData.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### Create receiveCommandData1
1. Create the device with device_id 'receiveCommandData1' on the dashboard.
2. Delete all text in the outbound_rules field
3. Create an inbound_rule with:
- command_id: "temp_in",
- data: ["temperature"],
- accept:
  - username: your username,
  - device_id: "sendCommandData1",
  - command_id: "temp_out"

```json
[
    {
        "command_id": "temp_in",
        "data": [
            "temperature"
        ],
        "max_freq": "0",
        "last_run": "1592996455835",
        "accept": [
            {
                "username": "***your username***",
                "device_id": "sendCommandData1",
                "command_id": "temp_out"
            }
        ]
    }
]
```

![receiveCommandData1](./receiveCommandData1.PNG "receiveCommandData1")
4. Click 'Create Device'
5. Download or copy the python file 'receiveCommand.py'
6. At the top of the python file, in device_settings, update the 4 values to match your device. Use the passwords provided for the device which can be found in the dashboard table or at the top of the create device page after creation.

#### RUN
1. Make sure the devices are set to active on the dashboard
2. CD to python file folder and run sendCommandData.py in a cmd window
```bash
python sendCommandData.py
```
3. CD to python file folder and run receiveCommandData.py in another cmd window
```bash
python receiveCommandData.py
```
On the sendCommandData.py window you should see 'Sending' printed every 10 seconds, while on receiveCommandData.py you should see the the full command object received followed by the command_id and then data.

You should be able to see that the command_id received is 'temp_in' while the command being sent out is 'temp_out' and the data key received is 'temperature', while 'temp' us sent out. This forms the basics of mapping between devices.

## Contributing
If you are a multilingual programmer and manage to translate these python examples in to another language, or produce some samples/tutorials of your own, please send them to hello@openrnd.co.uk and we'll add them here!

We are looking to make a code and device sharing platform in the near future.

## Acknowledgment
- Our IOT Network uses SocketIO as a base
- Big thanks to [makeareadme](https://www.makeareadme.com/) for making it super easy to make this README

## License
[MIT](https://choosealicense.com/licenses/mit/)
