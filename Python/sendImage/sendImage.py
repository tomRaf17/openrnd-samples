import socketio
import time

import cv2
from imutils.video import VideoStream
from imutils import resize
import numpy as np
import base64

###---Script Setup---###

vs = VideoStream().start()
time.sleep(1.5)

###---End Script Setup---###
###---Device Settings---###

##-- Update these values with the values set on the dashboard--##
device_settings = {'username': 'test@example.com', 'device_id': 'sendImage1', 'device_pw': '1234', 'server_pw': '1234'}

###---End Device Settings---###
###---SocketIO Setup---###

sio = socketio.Client();
error = None

@sio.on('connect')
def connect():
    print('Connected')
    sio.emit('authentication', {'username': device_settings['username'], 'device_id': device_settings['device_id'], 'password': device_settings['device_pw']})

@sio.on('authenticated')
def authenticated(data):
	if (data['s_password'] != device_settings['server_pw']):
		error = 'Server password mismatch'
		sio.disconnect()
	else:
		print('Authenticated')
		main()

@sio.on('unauthorized')
def unauthorized(reason):
    print('%s %s' % ('unauthorized:', reason))
    error = reason
    sio.disconnect()

@sio.on('disconnect')
def disconnect():
    print('%s %s' % ('disconnect:', (error)))

###---End SocketIO Setup---###
###---OpenRnD Settings---###

##-- Update here to handle inbound rules--##
@sio.on('action_in')
def action_in(action):
    print('%s %s' % ('action_in: ', action))
    print('%s' % action['command_id'])

##-- Update here to handle outbound rules--##
def action_out():
	while True:

		frame = vs.read()
		frame = resize(frame, width=800)
		cv2.imshow('Live view', frame)

		key = cv2.waitKey(1) & 0xff

		if key == ord("q"):
			break
		elif key == ord("t"):
			encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
			image = cv2.imencode('.jpg', frame, encode_param)[1].tostring()
			print('Sending')
			sio.emit('action_out', { 'command_id': 'image_out' , 'data': { 'image': image } })

	cv2.destroyAllWindows()
	vs.stop()

##--Main is run one the device is authenticated--##
def main():
	action_out()

###---End OpenRnD Settings---###
###---Connect to Network---###

sio.connect('https://staging.openrnd.co.uk')
#sio.connect('https://openrnd.co.uk')
#sio.connect('http://localhost:4200')

###---End Connect to Network---###
